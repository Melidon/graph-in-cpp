#pragma once

#include <vector>

template <typename G, typename VertexData, typename EdgeData>
concept Graph = requires(G graph, typename G::Id id, VertexData vertex_data, EdgeData edge_data) {
    { graph.add_vertex(vertex_data) } -> std::same_as<typename G::Id>;
    { graph.get_vertex_data(id) } -> std::same_as<VertexData &>;
    { graph.get_vertex_edges(id) } -> std::same_as<std::vector<typename G::Id>>;
    { graph.add_edge(edge_data, id, id) } -> std::same_as<void>;
    { graph.get_edge_data(id, id) } -> std::same_as<EdgeData &>;
};
