#pragma once

#include <deque>
#include <functional>
#include <set>

#include "../graph.hpp"

template <typename VertexData, typename EdgeData, Graph<VertexData, EdgeData> Graph>
void depthFirstSearch(Graph &graph, typename Graph::Id from, std::function<void(VertexData &)> visit) {
    if (!visit) {
        return;
    }
    auto visited = std::set<typename Graph::Id>{};
    auto queue = std::deque<typename Graph::Id>{from};
    while (!queue.empty()) {
        auto id = queue.front();
        queue.pop_front();
        visit(graph.get_vertex_data(id));
        visited.insert(id);
        for (auto edge : graph.get_vertex_edges(id)) {
            if (!visited.contains(edge)) {
                queue.push_front(edge);
            }
        }
    }
}
