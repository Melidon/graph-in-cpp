#pragma once

#include <boost/numeric/ublas/matrix.hpp>
#include <cstdint>
#include <optional>
#include <ranges>

template <typename VertexData, typename EdgeData> class AdjacencyMatrixGraph final {
public:
    using Id = std::uint64_t;

private:
    std::vector<VertexData> vertices_{};
    boost::numeric::ublas::matrix<std::optional<EdgeData>> edges_{};

public:
    [[nodiscard]] explicit AdjacencyMatrixGraph() = default;

    [[nodiscard]] Id add_vertex(VertexData data) {
        Id id = vertices_.size();
        vertices_.push_back(std::move(data));
        edges_.resize(edges_.size1() + 1, edges_.size2() + 1);
        return id;
    }

    [[nodiscard]] VertexData &get_vertex_data(Id id) noexcept {
        return vertices_[id];
    }

    [[nodiscard]] std::vector<Id> get_vertex_edges(Id from) {
        // If GCC would support C++23.
        /*
        return std::views::iota(0lu, vertices_.size())
            | std::views::filter([&edges_ = edges_, from](auto to) { return edges_(from, to).has_value(); })
            | std::ranges::to<std::vector>();
        */

        // For now...
        auto result = std::views::iota(0lu, vertices_.size()) | std::views::filter([&edges_ = edges_, from](auto to) { return edges_(from, to).has_value(); });
        return std::vector<Id>{result.begin(), result.end()};
    }

    void add_edge(EdgeData data, Id from, Id to) {
        edges_(from, to) = std::move(data);
    }

    [[nodiscard]] EdgeData &get_edge_data(Id from, Id to) noexcept {
        return edges_(from, to).value();
    }
};
