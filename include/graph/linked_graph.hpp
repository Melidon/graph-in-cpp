#pragma once

#include <cstdint>
#include <memory>
#include <ranges>
#include <vector>

template <typename VertexData, typename EdgeData> class LinkedGraph final {
public:
    using Id = std::uint64_t;

private:
    struct Edge;

    struct Vertex final {
        VertexData data_;
        Id id_;
        std::vector<std::shared_ptr<Edge>> edges_{};

        [[nodiscard]] explicit Vertex(VertexData data, Id id)
            : data_{std::move(data)}
            , id_{id} {
        }
    };

    struct Edge final {
        EdgeData data_;
        std::weak_ptr<Vertex> from_;
        std::weak_ptr<Vertex> to_;

        [[nodiscard]] explicit Edge(EdgeData data, std::shared_ptr<Vertex> from, std::shared_ptr<Vertex> to)
            : data_{std::move(data)}
            , from_{from}
            , to_{to} {
        }
    };

    std::vector<std::shared_ptr<Vertex>> vertices_{};

public:
    [[nodiscard]] explicit LinkedGraph() = default;

    [[nodiscard]] Id add_vertex(VertexData data) {
        Id id = vertices_.size();
        auto vertex = std::make_shared<Vertex>(std::move(data), id);
        vertices_.push_back(vertex);
        return id;
    }

    [[nodiscard]] VertexData &get_vertex_data(Id id) noexcept {
        return vertices_[id]->data_;
    }

    [[nodiscard]] std::vector<Id> get_vertex_edges(Id from) {
        // If GCC would support C++23.
        /*
        return vertices_[from]->edges_
            | std::views::transform([](auto edge) { return edge->to_.lock()->id_; });
            | std::ranges::to<std::vector>();
        */

        // For now...
        auto result = vertices_[from]->edges_ | std::views::transform([](auto edge) { return edge->to_.lock()->id_; });
        return std::vector<Id>{result.begin(), result.end()};
    }

    void add_edge(EdgeData data, Id from, Id to) {
        auto edge = std::make_shared<Edge>(std::move(data), vertices_[from], vertices_[to]);
        vertices_[from]->edges_.push_back(edge);
    }

    [[nodiscard]] EdgeData &get_edge_data(Id from, Id to) noexcept {
        auto result = std::ranges::find_if(vertices_[from]->edges_, [to](auto edge) { return edge->to_.lock()->id_ == to; });
        return result[0]->data_;
    }
};
