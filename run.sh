#!/bin/bash

clear
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING=Debug -B build && \
cmake --build build --config Debug --target graph_demo -- && \
build/graph_demo

