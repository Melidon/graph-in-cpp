#pragma once

#include <string>
#include <string_view>

class Location final {
private:
    std::string name_;

public:
    [[nodiscard]] explicit Location(std::string name) noexcept;

    [[nodiscard]] std::string_view get_name() const noexcept;
};
