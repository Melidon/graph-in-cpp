#include "location.hpp"

Location::Location(std::string name) noexcept
    : name_{std::move(name)} {
}

std::string_view Location::get_name() const noexcept {
    return std::string_view{name_};
}
