#include <iostream>

#include "graph/algorithms/breadth_first_search.hpp"
#include "graph/algorithms/depth_first_search.hpp"
#include "graph/linked_graph.hpp"
#include "location.hpp"
#include "road.hpp"

int main(int, char **) {
    auto map = LinkedGraph<Location, Road>{};

    auto Piliscsaba = map.add_vertex(Location{"Piliscsaba"});
    auto Tinnye = map.add_vertex(Location{"Tinnye"});
    auto Perbál = map.add_vertex(Location{"Perbál"});
    auto Tök = map.add_vertex(Location{"Tök"});
    auto Zsámbék = map.add_vertex(Location{"Zsámbék"});
    auto Biatorbágy = map.add_vertex(Location{"Biatorbágy"});
    auto Sóskút = map.add_vertex(Location{"Sóskút"});
    auto Érd = map.add_vertex(Location{"Érd"});

    map.add_edge(Road{5.5}, Piliscsaba, Tinnye);
    map.add_edge(Road{5.0}, Tinnye, Perbál);
    map.add_edge(Road{3.5}, Perbál, Tök);
    map.add_edge(Road{2.4}, Tök, Zsámbék);
    map.add_edge(Road{13.9}, Zsámbék, Biatorbágy);
    map.add_edge(Road{9.2}, Biatorbágy, Sóskút);
    map.add_edge(Road{7.9}, Sóskút, Érd);

    auto printLocationName = [](auto &vertex) { std::cout << vertex.get_name() << std::endl; };
    breadthFirstSearch<Location, Road, LinkedGraph<Location, Road>>(map, Piliscsaba, printLocationName);
    std::cout << std::endl;
    depthFirstSearch<Location, Road, LinkedGraph<Location, Road>>(map, Piliscsaba, printLocationName);

    return EXIT_SUCCESS;
}
