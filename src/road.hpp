#pragma once

class Road final {
private:
    double length_;

public:
    [[nodiscard]] explicit Road(double length) noexcept;

    [[nodiscard]] double get_length() const noexcept;
};
