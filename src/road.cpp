#include "road.hpp"

Road::Road(double length) noexcept
    : length_{length} {
}

double Road::get_length() const noexcept {
    return length_;
}
